﻿using AutoMapper;
using TradeProcessor.Core.Models;
using TradeProcessor.DataAccess.Dtos;

namespace TradeProcessor.DataAccess.Mapping
{
    public class TradeDtoProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<TradeRecord, TradeDto>();
        }
    }
}
