﻿using AutoMapper;
using NLog;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using TradeProcessor.Core.Models;
using TradeProcessor.DataAccess.Dtos;

namespace TradeProcessor.DataAccess.Repositories
{
    public class TradeRepository : ITradeRepository
    {
        private static ILogger _logger;

        public TradeRepository(ILogger logger)
        {
            _logger = logger;
        }

        public void Write(IEnumerable<TradeRecord> trades)
        {
            var tradeDtos = Mapper.Map<IList<TradeDto>>(trades);

            var cs = ConfigurationManager.ConnectionStrings["TradeDB"].ConnectionString;

            using (var connection = new SqlConnection(cs))
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction())
                {
                    foreach (var trade in trades)
                    {
                        var command = connection.CreateCommand();
                        command.Transaction = transaction;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "dbo.insert_trade";
                        command.Parameters.AddWithValue("@sourceCurrency", trade.SourceCurrency);
                        command.Parameters.AddWithValue("@destinationCurrency", trade.DestinationCurrency);
                        command.Parameters.AddWithValue("@lots", trade.Lots);
                        command.Parameters.AddWithValue("@price", trade.Price);

                        command.ExecuteNonQuery();
                    }

                    transaction.Commit();
                }
                connection.Close();
            }

            _logger.Info($"INFO: {tradeDtos.Count} trades processed");
        }
    }
}
