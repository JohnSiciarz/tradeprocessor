﻿using System.Collections.Generic;
using TradeProcessor.Core.Models;

namespace TradeProcessor.DataAccess.Repositories
{
    public interface ITradeRepository
    {
        void Write(IEnumerable<TradeRecord> trades);
    }
}
