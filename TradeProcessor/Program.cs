﻿using Autofac;
using System.IO;
using TradeProcessor.Ioc;
using TradeProcessor.Processors;

namespace TradeProcessor
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = ContainerConfig.Configure();
            using (var scope = container.BeginLifetimeScope())
            {
                var app = scope.Resolve<ITradeProcessor>();
                app.ProcessTrades(new MemoryStream());
            }
        }
    }
}
