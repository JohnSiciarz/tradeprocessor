﻿using System.IO;

namespace TradeProcessor.Processors
{
    interface ITradeProcessor
    {
        void ProcessTrades(Stream stream);
    }
}
