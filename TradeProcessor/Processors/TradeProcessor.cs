﻿using System.IO;
using TradeProcessor.Core.Parsers;
using TradeProcessor.DataAccess.Repositories;

namespace TradeProcessor.Processors
{
    public class TradeProcessor : ITradeProcessor
    {
        private static ITradeRecordParser _tradeRecordParser;
        private static ITradeRepository _tradeRepository;

        public TradeProcessor(ITradeRecordParser tradeRecordParser, ITradeRepository tradeRepository)
        {
            _tradeRecordParser = tradeRecordParser;
            _tradeRepository = tradeRepository;
        }

        public void ProcessTrades(Stream stream)
        {
            var trades = _tradeRecordParser.Parse(stream);

            _tradeRepository.Write(trades);
        }

    }
}
