﻿using Autofac;
using AutoMapper;
using NLog;
using System.Collections.Generic;
using TradeProcessor.Processors;
using TradeProcessor.Core.Parsers;
using TradeProcessor.DataAccess.Mapping;
using TradeProcessor.DataAccess.Repositories;

namespace TradeProcessor.Ioc
{
    public static class ContainerConfig
    {
        public static IContainer Configure()
        {
            var builder = new ContainerBuilder();

            builder.Register(c => LogManager.GetCurrentClassLogger()).As<ILogger>();

            builder.RegisterType<Processors.TradeProcessor>().As<ITradeProcessor>();
            builder.RegisterType<TradeRecordParser>().As<ITradeRecordParser>();
            builder.RegisterType<TradeRepository>().As<ITradeRepository>();

            builder.RegisterType<TradeDtoProfile>().As<Profile>();

            var container = builder.Build();

            var profiles = container.Resolve<IEnumerable<Profile>>();
            Mapper.Initialize(cfg =>
            {
                foreach (var profile in profiles)
                {
                    cfg.AddProfile(profile);
                }
            });

            return container;
        }
    }
}
