﻿using NLog;
using System.Collections.Generic;
using System.IO;
using TradeProcessor.Core.Models;

namespace TradeProcessor.Core.Parsers
{
    public class TradeRecordParser : ITradeRecordParser
    {
        private static ILogger _logger;

        private static readonly float LotSize = 100000f;

        public TradeRecordParser(ILogger logger)
        {
            _logger = logger;
        }

        public IEnumerable<TradeRecord> Parse(Stream stream)
        {
            var trades = new List<TradeRecord>();

            var lineCount = 1;

            using (var reader = new StreamReader(stream))
            {
                string line;

                while ((line = reader.ReadLine()) != null)
                {
                    var trade = ParseLine(line, lineCount);

                    if (trade != null)
                    {
                        trades.Add(trade);
                    }

                    lineCount++;
                }
            }

            return trades;
        }

        private TradeRecord ParseLine(string line, int lineCount)
        {
            var fields = line.Split(new char[] { ',' });

            if (!VerifyLineForm(fields, lineCount))
            {
                return null;
            }

            var tradeAmount = ParseTradeAmount(fields, lineCount);

            if (!tradeAmount.HasValue || !VerifyTradeAmount(tradeAmount.Value, lineCount))
            {
                return null;
            }

            var tradePrice = ParseTradePrice(fields, lineCount);

            if (!tradePrice.HasValue)
            {
                return null;
            }

            return new TradeRecord
            {
                SourceCurrency = fields[0].Substring(0, 3),
                DestinationCurrency = fields[0].Substring(3, 3),
                Lots = tradeAmount.Value / LotSize,
                Price = tradePrice.Value
            };
        }

        private bool VerifyLineForm(string[] fields, int lineCount)
        {
            bool isWellFormed = true;

            if (fields.Length != 3)
            {
                _logger.Warn($"Line {lineCount} malformed. Only {fields.Length} field(s) found.");
                isWellFormed = false;
            }

            if (fields[0].Length != 6)
            {
                _logger.Warn($"Trade currencies on line {lineCount} malformed: [{fields[0]}]");
                isWellFormed = false;
            }

            return isWellFormed;
        }

        private int? ParseTradeAmount(string[] fields, int lineCount)
        {
            int tradeAmount;
            if (!int.TryParse(fields[1], out tradeAmount))
            {
                _logger.Warn($"Trade amount on line {lineCount} is not a valid integer: [{fields[1]}]");
                return null;
            }

            return tradeAmount;
        }

        private decimal? ParseTradePrice(string[] fields, int lineCount)
        {
            decimal tradePrice;
            if (!decimal.TryParse(fields[2], out tradePrice))
            {
                _logger.Warn($"WARN: Trade price on line {lineCount} is not a valid decimal: [{fields[1]}]");
                return null;
            }

            return tradePrice;
        }

        private bool VerifyTradeAmount(int tradeAmount, int lineCount)
        {
            if (tradeAmount < 0)
            {
                _logger.Warn($"WARN: Trade amount on line {lineCount} has a negative value: [{tradeAmount}]");
                return false;
            }

            return true;
        }
    }
}
