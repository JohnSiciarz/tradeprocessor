﻿using System.Collections.Generic;
using System.IO;
using TradeProcessor.Core.Models;


namespace TradeProcessor.Core.Parsers
{
    public interface ITradeRecordParser
    {
        IEnumerable<TradeRecord> Parse(Stream stream);
    }
}
